package Algorithms;

import Models.Tuple;
import Utilities.ProgramType;

import java.util.*;

/**
 * Created by Alexandra Muresan on 17-Dec-17.
 */
public class ProductionString {

    private ParsingTable parsingTable;
    private String sequence;
    private Stack sequenceStack;
    private Stack workingStack;
    private List<Integer> prodNumbers;

    public ProductionString(String sequence, ProgramType programType) {
        this.parsingTable = new ParsingTable(programType);
        this.sequence = sequence;
        this.sequenceStack = new Stack();
        this.workingStack = new Stack();
        this.prodNumbers = new ArrayList<>();
        if (programType.equals(ProgramType.SIMPLE_GRAMMAR)) {
            initializeStacks();
        }
        if (programType.equals(ProgramType.COMPLEX_GRAMMAR)) {
            complexInitStacks();
        }
    }

    public void initializeStacks() {
        for(int i = sequence.length() - 1 ; i >=0 ; i--) {
            sequenceStack.push(String.valueOf(sequence.charAt(i)));
        }
        workingStack.push("S");
    }

    private void complexInitStacks() {
        String[] splitSequence = sequence.split(" ");
        for (int i = splitSequence.length - 1; i>= 0; i--) {
            sequenceStack.push(splitSequence[i]);
        }
        workingStack.push("$");
        workingStack.push("start");
    }

    public void processSequence() throws Exception {
        while(!sequenceStack.isEmpty() || !workingStack.isEmpty()) {
            Tuple<String,String> key = new Tuple<>((String)workingStack.peek(),(String)sequenceStack.peek());
            if(parsingTable.checkIfContains(key)) {
                if(parsingTable.getValueForKey(key).getKey().compareTo("err") != 0) {
                    if(parsingTable.getValueForKey(key).getKey().compareTo("pop") == 0) {
                        sequenceStack.pop();
                        workingStack.pop();
                    } else {
                        Tuple<String,Integer> value = parsingTable.getValueForKey(key);
                        workingStack.pop();
                        if(value.getKey().compareTo("e") != 0) {
//                            for(int i = value.getKey().length() - 1; i >=0 ; i--) {
//                                workingStack.push(String.valueOf(value.getKey().charAt(i)));
//                            }
                            List<String> split = new ArrayList<>();
                            String[] splitted = value.getKey().split(" ");
                            for (String s : splitted) {
                                split.add(s);
                            }
                            Collections.reverse(split);
                            for (String s : split) {
                                workingStack.push(s);
                            }
                        }
                        prodNumbers.add(value.getValue());
                    }
                } else {
                    throw new Exception("Error at " + key.getValue());
                }
            } else {
                throw new Exception("Error at " + key.getValue());
            }
        }
    }

    public Stack getSequenceStack() {
        return sequenceStack;
    }

    public Stack getWorkingStack() {
        return workingStack;
    }

    public List<Integer> getProdNumbers() {
        return prodNumbers;
    }
}
