package Algorithms;

import Models.Production;
import Models.Tuple;
import Utilities.ProgramType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexandra Muresan on 16-Dec-17.
 */
public class ParsingTable {

    private First first;
    private Follow follow;
    private HashMap<Tuple<String, String>, Tuple<String, Integer>> parsingTable;

    public ParsingTable(ProgramType programType) {
        first = new First(programType);
        follow = new Follow.Builder().programType(programType).grammar(first.getGrammar()).first(first).follow(new HashMap<>()).build();
        parsingTable = new HashMap<>();
        initTableKeys();
        if (programType.equals(ProgramType.SIMPLE_GRAMMAR)) {
            completeTable();
        }
        if (programType.equals(ProgramType.COMPLEX_GRAMMAR)) {
            complexeCompleteTable();
        }
    }

    public void initTableKeys() {
        for (String nonTerminal : first.getGrammar().getNonTerminals()) {
            for (String terminal : first.getGrammar().getTerminals()) {
                Tuple<String, String> key = new Tuple<>(nonTerminal, terminal);
                parsingTable.put(key, new Tuple<>("err", 0));
            }
            parsingTable.put(new Tuple<>(nonTerminal, "$"), new Tuple<>("err", 0));
        }
        for (String terminal : first.getGrammar().getTerminals()) {
            for (String term : first.getGrammar().getTerminals()) {
                if (terminal.compareTo(term) == 0) {
                    parsingTable.put(new Tuple<>(terminal, term), new Tuple<>("pop", 0));
                }
            }
            parsingTable.put(new Tuple<>(terminal, "$"), new Tuple<>("err", 0));
        }
        parsingTable.put(new Tuple<>("$", "$"), new Tuple<>("acc", 0));
    }

    public void printParsingTable() {
        for (Tuple<String, String> key : parsingTable.keySet()) {
            System.out.println(key + " => " + parsingTable.get(key));
        }
    }

    public Set<String> rhsFirstProduction(Production p) {
        Set<String> result = new HashSet<>();
        if (String.valueOf(p.getTransitions().get(0).charAt(0)).compareTo("e") == 0) {
            result.add("e");
        } else if (first.getGrammar().getTerminals().contains(String.valueOf(p.getTransitions().get(0).charAt(0)))) {
            result.add(String.valueOf(p.getTransitions().get(0).charAt(0)));
        } else {
            result.addAll(first.getFirst().get(String.valueOf(p.getTransitions().get(0).charAt(0))));
        }
        return result;
    }

    private Set<String> complexRhsFirstProduction(Production production) {
        Set<String> result = new HashSet<>();
        String[] transitionSplit = production.getTransitions().get(0).split(" ");
        if (transitionSplit[0].equals("e")) {
            result.add("e");
        } else {
            if (first.getGrammar().getTerminals().contains(transitionSplit[0])) {
                result.add(transitionSplit[0]);
            } else {
                if (first.getFirst().get(transitionSplit[0]) != null) {
                    result.addAll(first.getFirst().get(transitionSplit[0]));
                } else {
                }
            }
        }
        return result;
    }

    public void completeTable() {
        for (Tuple<String, String> key : parsingTable.keySet()) {
            List<Production> productions = first.getGrammar().getProductionForNonTerminal(key.getKey());
            for (Production p : productions) {
                Set<String> firsts = rhsFirstProduction(p);
                System.out.println("RHS First Production (" + p + ") = " + firsts);
                if (firsts.contains(key.getValue()) && !firsts.contains("e")) {
                    parsingTable.replace(key, new Tuple<>(p.getTransitions().get(0), p.getNumber()));
                } else if (firsts.contains("e")) {
                    if (key.getValue().compareTo("$") == 0) {
                        parsingTable.replace(key, new Tuple<>("e", p.getNumber()));
                    }
                    Set<String> follows = follow.getFollow().get(key.getKey());
                    for (String f : follows) {
                        if (key.getValue().compareTo(f) == 0) {
                            parsingTable.replace(key, new Tuple<>("e", p.getNumber()));
                        }
                    }
                }
            }
            System.out.println();
        }

    }

    private void complexeCompleteTable() {
        for (Tuple<String, String> key : parsingTable.keySet()) {
            List<Production> productions = first.getGrammar().getProductionForNonTerminal(key.getKey());
            for (Production production : productions) {
                Set<String> firsts = complexRhsFirstProduction(production);
                if (firsts.contains(key.getValue()) && !firsts.contains("e")) {
                    StringBuilder finalTransition = new StringBuilder();
                    for (String transition : production.getTransitions()) {
                        finalTransition.append(transition);
                    }
                    parsingTable.replace(key, new Tuple<>(finalTransition.toString(), production.getNumber()));
                } else {
                    if (firsts.contains("e")) {
                        if (key.getValue().equals("$")) {
                            parsingTable.replace(key, new Tuple<>("e", production.getNumber()));
                        }
                        Set<String> follows = follow.getFollow().get(key.getKey());
                        for (String followComp : follows) {
                            if (key.getValue().equals(followComp)) {
                                parsingTable.replace(key, new Tuple<>("e", production.getNumber()));
                            }
                        }
                    }
                }
            }
        }
    }

    public HashMap<Tuple<String, String>, Tuple<String, Integer>> getParsingTable() {
        return parsingTable;
    }

    public boolean checkIfContains(Tuple<String, String> key) {
        for (Tuple<String, String> k : parsingTable.keySet()) {
            if (k.getValue().equals(key.getValue())) {
                System.out.println(k);
            }
            if (k.getKey().compareTo(key.getKey()) == 0 && k.getValue().compareTo(key.getValue()) == 0) {
                return true;
            }
        }
        return false;
    }

    public Tuple<String, Integer> getValueForKey(Tuple<String, String> key) {
        for (Tuple<String, String> k : parsingTable.keySet()) {
            if (k.getKey().compareTo(key.getKey()) == 0 && k.getValue().compareTo(key.getValue()) == 0) {
                if(!parsingTable.get(k).getKey().equals("err")){
                    return parsingTable.get(k);
                }
            }
        }
        return new Tuple<String, Integer>("err", 0);
    }

    @Override
    public String toString() {
        return "ParsingTable{" +
                "first=" + first + "\n, " +
                "follow=" + follow + "\n, " +
                "parsingTable=" + parsingTable +
                '}';
    }
}

