package Algorithms;

import Models.Grammar;
import Models.Production;
import Utilities.GrammarParser;
import Utilities.ProgramType;

import java.util.*;

/**
 * Created by Alexandra Muresan on 05-Dec-17.
 */
public class First {

    private Grammar grammar;
    private Map<String, Set<String>> first;
    private ProgramType programType;

    public First(ProgramType programType) {
        this.programType = programType;
        GrammarParser parser = new GrammarParser("programGrammar.txt");
        this.grammar = parser.getGrammar();
        this.first = new HashMap<>();
        initializeFirst();
        computeFirst();
    }

    public Map<String, Set<String>> getFirst() {
        return first;
    }

    public void setFirst(Map<String, Set<String>> first) {
        this.first = first;
    }

    public void initializeFirst() {
        for (String nonTerminal : this.grammar.getNonTerminals()) {
            first.put(nonTerminal, new HashSet<>());
        }
    }

    public void computeFirst() {
        if (programType == ProgramType.SIMPLE_GRAMMAR) {
            computeRule1();
            computeRule2();
        }
        if (programType == ProgramType.COMPLEX_GRAMMAR) {
            complexComputeRule1();
            complexComputeRule2();
        }
    }

    // Adauga epsilon la first pentru fiecare nonterminal care are productie cu epsilon
    // Cauta productii cu primul element terminal si il adauga la first-ul nonterminalului corespunzator
    public void computeRule1() {
        for (Production p : this.grammar.getProductions()) {
            Set<String> firsts = first.get(p.getInitialState());
            List<String> transitions = p.getTransitions();
            for (String t : transitions) {
                if (t.charAt(0) == 'e') {
                    firsts.add("e");
                    first.put(p.getInitialState(), firsts);
                }

                if (this.grammar.getTerminals().contains(String.valueOf(t.charAt(0)))) {
                    firsts.add(String.valueOf(t.charAt(0)));
                    first.replace(p.getInitialState(), firsts);
                }
            }
        }
    }

    private void complexComputeRule1() {
        for (Production p : this.grammar.getProductions()) {
            Set<String> firsts = first.get(p.getInitialState());
            List<String> transitions = p.getTransitions();
            for (String t : transitions) {
                String[] splitTransition = t.split(" ");
                if (Objects.equals(splitTransition[0], "e")) {
                    firsts.add("e");
                    first.put(p.getInitialState(), firsts);
                }

                if (this.grammar.getTerminals().contains(splitTransition[0])) {
                    firsts.add(splitTransition[0]);
                    first.replace(p.getInitialState(), firsts);
                }
            }
        }
    }

    // Ia celelalte productii pentru care nu au fost respectate regulile de mai sus
    public void computeRule2() {
        for (Production p : this.grammar.getProductions()) {
            Set<String> firsts = new HashSet<>();
            List<String> transitions = p.getTransitions();
            for (String t : transitions) {
                if (!(this.grammar.getTerminals().contains(String.valueOf(t.charAt(0))) || String.valueOf(t.charAt(0)).compareTo("e") == 0)) {
                    firsts = first.get(String.valueOf(t.charAt(0)));
                    first.replace(p.getInitialState(), firsts);
                }
            }
        }

        // Verifica daca au mai ramas nonterminale necompletate
        for (Production p : this.grammar.getProductions()) {
            if (first.get(p.getInitialState()).isEmpty()) {
                List<String> trans = p.getTransitions();
                Set<String> firsts = new HashSet<>();
                for (String t : trans) {
                    if (!(this.grammar.getTerminals().contains(String.valueOf(t.charAt(0))) || String.valueOf(t.charAt(0)).compareTo("e") == 0)) {
                        firsts = first.get(String.valueOf(t.charAt(0)));
                        first.replace(p.getInitialState(), firsts);
                    }
                }
            }
        }
    }

    private void complexComputeRule2() {
        for (Production p : this.grammar.getProductions()) {
            Set<String> firsts = new HashSet<>();
            List<String> transitions = p.getTransitions();
            for (String t : transitions) {
                String[] splitTransition = t.split(" ");
                if (!(this.grammar.getTerminals().contains(splitTransition[0]) || splitTransition[0].compareTo("e") == 0)) {
                    firsts = first.get(splitTransition[0]);
                    first.replace(p.getInitialState(), firsts);
                }
            }
        }

        // Verifica daca au mai ramas nonterminale necompletate
        for (Production p : this.grammar.getProductions()) {
            if (first.get(p.getInitialState()) == null || first.get(p.getInitialState()).isEmpty()) {
                List<String> trans = p.getTransitions();
                Set<String> firsts = new HashSet<>();
                for (String t : trans) {
                    String[] splitTransition = t.split(" ");
                    if (!(this.grammar.getTerminals().contains(splitTransition[0]) || splitTransition[0].compareTo("e") == 0)) {
                        firsts = first.get(splitTransition[0]);
                        first.replace(p.getInitialState(), firsts);
                    }
                }
            }
        }
    }

    public void printFirst() {
        System.out.println("First");
        for (String key : first.keySet()) {
            System.out.println(key);
            Set<String> result = first.get(key);
            for (String s : result) {
                System.out.print(s + " ");
            }
            System.out.println();
        }
    }

    @Override
    public String toString() {
        return "First{" +
                "first=" + first +
                '}';
    }

    public Set<String> first(String character) {
        Set<String> result = this.first.get(character);
        if (result == null) {
            result = new HashSet<>();
        }
        return result;
    }

    public Grammar getGrammar() {
        return grammar;
    }
}
