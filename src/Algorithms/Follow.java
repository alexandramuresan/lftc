package Algorithms;

import Models.Grammar;
import Models.Production;
import Utilities.ProgramType;

import java.util.*;

/**
 * Created by Raul on Dec, 2017
 */
public class Follow {

    private Grammar grammar;
    private Map<String, Set<String>> followMap;
    private First first;
    private ProgramType programType;

    private Follow() {
        this.followMap = new HashMap<>();
    }

    public ProgramType getProgramType() {
        return programType;
    }

    public void setProgramType(ProgramType programType) {
        this.programType = programType;
    }

    public Grammar getGrammar() {
        return grammar;
    }

    public void setGrammar(Grammar grammar) {
        this.grammar = grammar;
    }

    public Map<String, Set<String>> getFollow() {
        return followMap;
    }

    public void setFollow(Map<String, Set<String>> follow) {
        this.followMap = follow;
    }

    public First getFirst() {
        return first;
    }

    public void setFirst(First first) {
        this.first = first;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Follow follow = (Follow) o;

        if (grammar != null ? !grammar.equals(follow.grammar) : follow.grammar != null) return false;
        return followMap != null ? followMap.equals(follow.followMap) : follow.followMap == null;
    }

    @Override
    public int hashCode() {
        int result = grammar != null ? grammar.hashCode() : 0;
        result = 31 * result + (followMap != null ? followMap.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Follow{" +
                "followMap=" + followMap +
                '}';
    }

    public void init() {
        for (String nonTerminal : this.grammar.getNonTerminals()) {
            if (nonTerminal.equals(Grammar.startSymbol)) {
                Set<String> terminals = new HashSet<>();
                terminals.add("$");
                followMap.put(nonTerminal, terminals);
            } else {
                followMap.put(nonTerminal, new HashSet<>());
            }
        }
    }

    private void computeFollow() {
        System.out.println();
        System.out.println("COMPUTE FOLLOW!!");
        Boolean modified = true;
        while (modified) {
            modified = false;
            for (Production production : this.grammar.getProductions()) {
                for (String transition : production.getTransitions()) {
                    String newTransition = parseTransition(transition);
                    System.out.println("Transition to parse: " + transition);
                    while (!newTransition.isEmpty()) {
                        char[] charArray = newTransition.toCharArray();
                        System.out.println("Transation parsed: " + newTransition);

                        // If case: A -> aB
                        if (charArray.length == 1) {
                            Set<String> newFollow = followMap.get(String.valueOf(charArray[0]));
                            newFollow.addAll(followMap.get(production.getInitialState()));
                            followMap.replace(String.valueOf(charArray[0]), newFollow);
                            System.out.println("1 Follow(" + String.valueOf(charArray[0]) + ") = " + newFollow);
                        } else {

                            // If case: A -> (S) || A -> S+
                            if (charArray.length == 2 && isTerminal(String.valueOf(charArray[1]))) {
                                Set<String> newFollow = followMap.get(String.valueOf(charArray[0]));
                                newFollow.add(String.valueOf(charArray[1]));
                                followMap.replace(String.valueOf(charArray[0]), newFollow);
                                System.out.println("2 Follow(" + String.valueOf(charArray[0]) + ") = " + newFollow);
                            } else {

                                // A -> aBb
                                Set<String> newFollow = followMap.get(String.valueOf(charArray[0]));
                                Set<String> oldFollow = new HashSet<>();
                                oldFollow.addAll(newFollow);
                                Set<String> firstResult = first.first(String.valueOf(charArray[1]));
                                for (String str : firstResult) {
                                    if (!str.equals("e")) {
                                        newFollow.add(str);
                                    }
                                }
                                if (firstResult.contains("e")) {
                                    System.out.println("First(" + charArray[1] + ") contains e");
                                    Set<String> followInitState = followMap.get(production.getInitialState());
                                    firstResult.addAll(followInitState);
                                }
                                followMap.replace(String.valueOf(charArray[0]), newFollow);
                                System.out.println("3 Follow(" + String.valueOf(charArray[0]) + ") = " + newFollow);
                                if (checkIfModified(oldFollow, newFollow)) {
                                    modified = true;
                                }
                            }
                        }

                        // Search for next NonTerminal in current transition
                        newTransition = newTransition.substring(1);
                        newTransition = parseTransition(newTransition);

                    }
                    System.out.println();
                }
            }
        }
    }

    private void complexComputeFollow() {
        System.out.println();
        Boolean modified = true;
        while (modified) {
            modified = false;
            for (Production production : this.grammar.getProductions()) {
                for (String transition : production.getTransitions()) {
                    String[] splitTransition = transition.split(" ");
                    List<String> newTransition = complexParseTransition(Arrays.asList(splitTransition));
                    while (!newTransition.isEmpty()) {

                        // If case: A -> aB
                        if (newTransition.size() == 1) {
                            Set<String> newFollow = followMap.get(newTransition.get(0));
                            newFollow.addAll(followMap.get(production.getInitialState()));
                            followMap.replace(newTransition.get(0), newFollow);
                        } else {

                            // If case: A -> (S) || A -> S+
                            if (newTransition.size() == 2 && isTerminal(newTransition.get(1))) {
                                Set<String> newFollow = followMap.get(newTransition.get(0));
                                newFollow.add(newTransition.get(1));
                                followMap.replace(newTransition.get(0), newFollow);
                            } else {

                                // A -> aBb
                                Set<String> newFollow = followMap.get(newTransition.get(0));
                                Set<String> oldFollow = new HashSet<>();
                                oldFollow.addAll(newFollow);
                                Set<String> firstResult = first.first(newTransition.get(1));
                                for (String str : firstResult) {
                                    if (!str.equals("e")) {
                                        newFollow.add(str);
                                    }
                                }
                                if (firstResult.contains("e")) {
                                    Set<String> followInitState = followMap.get(production.getInitialState());
                                    firstResult.addAll(followInitState);
                                }
                                followMap.replace(newTransition.get(0), newFollow);
                                if (checkIfModified(oldFollow, newFollow)) {
                                    modified = true;
                                }
                            }
                        }

                        // Search for next NonTerminal in current transition
                        newTransition.remove(0);
                        newTransition = complexParseTransition(newTransition);

                    }
                }
            }
        }
    }

    private Boolean checkIfModified(Set<String> oldFollow, Set<String> newFollow) {
        if (oldFollow.size() != newFollow.size()) {
            return true;
        }
        Boolean modified = false;
        Iterator<String> iteratorOld = oldFollow.iterator();
        Iterator<String> iteratorNew = newFollow.iterator();
        while (!modified && iteratorOld.hasNext() && iteratorNew.hasNext()) {
            if (!iteratorOld.next().equals(iteratorNew.next())) {
                modified = true;
            }
        }
        return modified;
    }

    private Boolean isNonTerminal(String character) {
        return this.grammar.getNonTerminals().contains(character);
    }

    private Boolean isTerminal(String character) {
        return this.grammar.getTerminals().contains(character);
    }

    private String parseTransition(String transition) {
        StringBuilder newTransition = new StringBuilder();
        newTransition.append("");
        Boolean nonTerminalFound = false;
        for (Character character : transition.toCharArray()) {
            if (isNonTerminal(character.toString())) {
                newTransition.append(character);
                nonTerminalFound = true;
            } else {
                if (nonTerminalFound) {
                    newTransition.append(character);
                }
            }
        }
        return newTransition.toString();
    }

    private List<String> complexParseTransition(List<String> transition) {
        List<String> newTransition = new ArrayList<>();
        Boolean nonTerminalFound = false;
        for (String character : transition) {
            if (isNonTerminal(character)) {
                newTransition.add(character);
                nonTerminalFound = true;
            } else {
                if (nonTerminalFound) {
                    newTransition.add(character);
                }
            }
        }
        return newTransition;
    }

    private Set<String> follow(String character) {
        Set<String> result = followMap.get(character);
        if (result == null) {
            result = new HashSet<>();
        }
        return result;
    }

    public static class Builder {
        private final Follow.FollowParams P;

        public Builder() {
            P = new Follow.FollowParams();
        }

        public Builder programType(ProgramType programType) {
            P.programType = programType;
            return this;
        }

        public Builder grammar(Grammar grammar) {
            P.grammar = grammar;
            return this;
        }

        public Builder follow(Map<String, Set<String>> followMap) {
            P.followMap = followMap;
            return this;
        }

        public Builder first(First first) {
            P.first = first;
            return this;
        }

        public Follow build() {
            Follow follow = new Follow();
            P.apply(follow);
            follow.init();
            try {
                if (follow.programType.equals(ProgramType.SIMPLE_GRAMMAR)) {
                    follow.computeFollow();
                }
                if (follow.programType.equals(ProgramType.COMPLEX_GRAMMAR)) {
                    follow.complexComputeFollow();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return follow;
        }
    }

    private static class FollowParams {
        Grammar grammar;
        Map<String, Set<String>> followMap;
        First first;
        ProgramType programType;

        FollowParams() {
        }

        void apply(Follow follow) {
            if (programType != null) {
                follow.setProgramType(programType);
            }
            if (grammar != null) {
                follow.setGrammar(grammar);
            }
            if (followMap != null) {
                follow.setFollow(followMap);
            }
            if (first != null) {
                follow.setFirst(first);
            }
        }
    }
}