package Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandra Muresan on 05-Dec-17.
 */
public class Production {

    private String initialState;
    private List<String> transitions;
    private Integer number;

    public Production(String initialState) {
        this.initialState = initialState;
        transitions = new ArrayList<>();
    }

    public void addTransition(String transition) {
        transitions.add(transition);
    }

    public String getInitialState() {
        return initialState;
    }

    public List<String> getTransitions() {
        return transitions;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public String toString() {
        String result = initialState + "->";
        for(int i=0;i<transitions.size();i++){
            result += transitions.get(i) + "(" + number + ")";
        }
        return result;
    }
}
