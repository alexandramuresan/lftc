package Models;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTree {

    private Node root;
    public List<Node> parents = new ArrayList<>();

    public BinaryTree(Node data) {
        this.root = data;
    }

    public BinaryTree() {
        this.root = null;
    }

    //Adds a new node to the tree, first checks the lexicographical order
    public void add(Node parent, Node child) {

        if (parent.getData().getKey().compareTo(child.getData().getKey()) > 0) {
            parent.setLeft(child);
            parents.add(child);
        } else {
            parent.setRight(child);
            parents.add(child);
        }
    }


    //Traverses the binary tree and returns all the nodes
    public List<Tuple<String, Integer>> bfs(Node root) {
        Queue<Node> q = new LinkedList<Node>();
        List<Tuple<String, Integer>> nodes = new ArrayList<>();
        if (root == null)
            return null;
        q.add(root);
        while (!q.isEmpty()) {
            Node n = (Node) q.remove();
            nodes.add(n.getData());
            if (n.getLeft() != null)
                q.add(n.getLeft());
            if (n.getRight() != null)
                q.add(n.getRight());
        }

        return nodes;
    }

    public Node getRoot() {
        return this.root;
    }

    //If root of the tree is not set
    public void setRoot(Node root) {
        parents.add(root);
        this.root = root;
    }

    //Checks if the tree already has that node or not
    public boolean checkIfContains(String element) {

        for (Node item : parents) {
            if (item.getData().getKey().equals(element)) {
                return true;
            }
        }
        return false;
    }
}
