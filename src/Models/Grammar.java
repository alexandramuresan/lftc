package Models;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Alexandra Muresan on 05-Dec-17.
 */
public class Grammar {
    public static String startSymbol = "S";

    private List<String> terminals;
    private List<String> nonTerminals;
    private List<Production> productions;

    public Grammar() {
        this.terminals = new ArrayList<>();
        this.nonTerminals = new ArrayList<>();
        this.productions = new ArrayList<>();
    }

    public void addTerminal(String terminal) {
        terminals.add(terminal);
    }

    public void addNonTerminal(String nonTerminal) {
        nonTerminals.add(nonTerminal);
    }

    public void addProduction(Production p) {
        productions.add(p);
    }

    public List<String> getTerminals() {
        return terminals;
    }

    public List<String> getNonTerminals() {
        return nonTerminals;
    }

    public List<Production> getProductions() {
        return productions;
    }

    public List<Production> getProductionForNonTerminal(String nonterminal) {
        List<Production> result = new ArrayList<>();
        for(Production p : productions) {
            if(p.getInitialState().compareTo(nonterminal)==0) {
                result.add(p);
            }
        }
        return result;
    }
}
