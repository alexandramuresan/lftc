package Models;

/**
 * Created by Alexandra Muresan on 16-Dec-17.
 */
public class Tuple<T,V> {

    private T key;
    private V value;

    public Tuple(T key, V value) {
        this.key = key;
        this.value = value;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "(" + key + "," + value + ")";
    }

    public boolean equals(Tuple<T,V> obj) {
        return this.getKey() == obj.getKey() && this.getValue() == obj.getValue();
    }
}
