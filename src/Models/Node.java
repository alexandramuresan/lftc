package Models;

//Class used to represent one component from a binary tree
public class Node {

    private Tuple<String,Integer> data;
    private Node left;
    private Node right;

    public Node(Tuple<String,Integer> data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public void setData(Tuple<String,Integer> data) {
        this.data = data;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Tuple<String,Integer> getData() {
        return data;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
}
