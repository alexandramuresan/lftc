package Utilities;

/**
 * Created by Raul on Dec, 2017
 */
public enum ProgramType {
    SIMPLE_GRAMMAR, COMPLEX_GRAMMAR
}