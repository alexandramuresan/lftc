package Utilities;

import Models.Grammar;
import Models.Production;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Alexandra Muresan on 05-Dec-17.
 */
public class GrammarParser {

    private String fileName;
    private Grammar grammar;

    public GrammarParser(String fileName) {
        this.fileName = fileName;
        this.grammar = new Grammar();
        processFile();
    }

    private void processNonTerminalsLine(String line) {
       String[] nonTerminals = line.split(",");
       for(int i = 0;i< nonTerminals.length;i++) {
           this.grammar.addNonTerminal(nonTerminals[i]);
       }
    }

    private void processTerminalsLine(String line) {
        String[] terminals = line.split(",");
        for(int i=0;i<terminals.length;i++){
            this.grammar.addTerminal(terminals[i]);
        }
    }

    private void processProduction(String production) {
        String[] firstParse = production.split("->");
        String[] transitions = firstParse[1].split("\\|");
        for(int i = 0;i<transitions.length;i++){
            Production p = new Production(firstParse[0]);
            p.addTransition(transitions[i]);
            p.setNumber(grammar.getProductions().size()+1);
            grammar.addProduction(p);
        }
    }

    public void processFile() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(fileName));
        } catch(IOException ex) {
            ex.printStackTrace();
        }
        if(scanner.hasNext()) {
            processNonTerminalsLine(scanner.nextLine());
        }
        if(scanner.hasNext()) {
            processTerminalsLine(scanner.nextLine());
        }
        while(scanner.hasNext()) {
            processProduction(scanner.nextLine());
        }
    }

    public Grammar getGrammar() {
        return grammar;
    }
}
