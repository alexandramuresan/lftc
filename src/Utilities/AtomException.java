package Utilities;

/**
 * Created by Alexandra Muresan on 30-Oct-17.
 */
public class AtomException extends Exception {

    private String message;

    public AtomException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}