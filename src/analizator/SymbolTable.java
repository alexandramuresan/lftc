package analizator;

import Models.BinaryTree;
import Models.Node;
import Models.Tuple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

//The symbol table is represented as an lexically ordered binary tree
public class SymbolTable {

    private BinaryTree binaryTree;
    private String fileName;

    public SymbolTable(String fileName) {
        this.binaryTree = new BinaryTree();
        this.fileName = fileName;
    }

    //Adds a new element only if it is not already stored
    public void addElement(String element) {
        if (binaryTree.getRoot() == null) {
            binaryTree.setRoot(new Node(new Tuple<>(element, 0)));
        } else {
            if (!binaryTree.checkIfContains(element)) {

                Integer lastPosition = binaryTree.parents.size() - 1;
                binaryTree.add(binaryTree.parents.get(lastPosition), new Node(new Tuple<>(element, lastPosition + 1)));

            }
        }
    }

    //Returns the position in the symbol table of a certain element (starting from 0)
    public Integer getElementPosition(String element) {

        List<Tuple<String, Integer>> nodes = binaryTree.bfs(binaryTree.getRoot());
        if (nodes != null) {
            for (Tuple<String, Integer> item : nodes) {
                if (item.getKey().equals(element)) {
                    return item.getValue();
                }
            }
        }
        return -2;
    }

    //Writes the symbol table to file
    public void writeSymbolTableToFile() throws Exception {
        File file = new File(fileName);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        List<Node> nodes = binaryTree.parents;
        if (!nodes.isEmpty()) {
            for (Node n : nodes) {
                bw.write(n.getData().getKey() + " " + n.getData().getValue());
                bw.newLine();
            }
        }
        bw.close();
    }
}