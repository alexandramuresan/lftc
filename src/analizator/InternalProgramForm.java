package analizator;

import Models.Tuple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class InternalProgramForm {

    private List<Tuple<Integer, Integer>> codes;
    private String fileName;

    public InternalProgramForm(String fileName) {
        this.codes = new ArrayList<>();
        this.fileName = fileName;
    }

    //Adds the code from the codification table or from the symbol table depending on the atom type
    public void add(Integer atomID, Integer stID) {
        Tuple<Integer, Integer> tuple = new Tuple<>(atomID, stID);
        codes.add(tuple);
    }

    //Writes the internal program form to file
    public void writeToFile() throws Exception {
        File file = new File(fileName);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (int i = 0; i < codes.size(); i++) {
            Tuple<Integer, Integer> tuple = codes.get(i);
            bw.write(tuple.getKey() + " " + tuple.getValue());
            bw.newLine();
        }
        bw.close();
    }
}