package analizator;

import Models.Tuple;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Alexandra Muresan on 25-Oct-17.
 */
// Reads the codification table from a file and stores the tuples into an array
public class CodificationTable {

    private String fileName;
    private List<Tuple<String, Integer>> codes;
    private List<String> operators;
    private List<String> keywords;
    private List<String> separators;
    public String identifierRegEx;
    public String numberConstantRegEx;
    public String stringConstantRegEx;

    public CodificationTable(String fileName) throws Exception {
        this.fileName = fileName;
        this.codes = new ArrayList<>();
        this.operators = Arrays.asList("+", "-", "*", "=", "==", "<", ">", ">=", "<=", "%", "/", "&&", "||", "->");
        this.keywords = Arrays.asList("program", "start", "let", "var", "int", "string", "bool", "read", "write", "if", "else", "while");
        this.separators = Arrays.asList(":", "[", "]", "(", ")", "{", "}");
        this.identifierRegEx = "[a-z]+[a-z]*[A-Z]*[a-z]*[A-Z]*[0-9]*";
        this.numberConstantRegEx = "[1-9]+[0-9]*|-[1-9]+[0-9]*|0";
        this.stringConstantRegEx = "\\\"[a-z]+[a-z]*\\\"|\\\"[A-Z]+[A-Z]*\\\"";
        readFromFile();

    }

    public List<Tuple<String, Integer>> getCodes() {
        return codes;
    }

    //Returns the code from the file corrresponding to a certain element
    public Integer getCode(String item) {
        for (Tuple<String, Integer> tuple : codes) {
            if (tuple.getKey().compareTo(item) == 0) {
                return tuple.getValue();
            }
        }
        return -2;
    }

    //Reads the codes from the file and stores the tuple <code,index> in an array
    public void readFromFile() throws Exception {
        Scanner scanner = new Scanner(new File(fileName));
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] elements = line.split(" ");
            Tuple<String, Integer> tuple = new Tuple<>(elements[0], Integer.parseInt(elements[1]));
            codes.add(tuple);
        }
        scanner.close();
    }

    //Checks if a certain atom is an operator
    public Integer checkIfOperator(String item) {
        for (String operator : operators) {
            if (operator.equals(item)) {
                return getCode(item);
            }
        }
        return -2;
    }

    //Checks if a certain atom is a keyword
    public Integer checkIfKeyword(String item) {
        for (String keyword : keywords) {
            if (keyword.equals(item)) {
                return getCode(item);
            }
        }
        return -2;
    }

    //Checks if a certain atom is a separator
    public Integer checkIfSeparator(String item) {
        for (String separator : separators) {
            if (separator.compareTo(item) == 0) {
                return getCode(item);
            }
        }
        return -2;
    }

    //Checks if an atom contains a separator in order to split it
    public String checkIfContainsSeparator(String atom) {

        for (Character item : atom.toCharArray()) {
            if (checkIfSeparator(item.toString()) != -2) {
                return item.toString();
            }
        }
        return "";
    }

    //Checks if an atom contains an operator in order to split it
    public String checkIfContainsOperator(String atom) {
        String op = "";
        for (Character item : atom.toCharArray()) {
            if (checkIfOperator(item.toString()) != -2) {
                op += item.toString();
            }
        }
        return op;
    }


}
