package analizator;

import Utilities.AtomException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Processor {
    private SymbolTable symbolTable;
    private InternalProgramForm internalProgramForm;
    private CodificationTable codificationTable;
    private List<String> atoms = new ArrayList<>();

    public Processor(String codificationFile, String symbolFile, String ipfFile, String programFile) throws Exception {
        symbolTable = new SymbolTable(symbolFile);
        internalProgramForm = new InternalProgramForm(ipfFile);
        codificationTable = new CodificationTable(codificationFile);
        processFile(programFile);
    }

    //Takes each line in order to split it into atoms
    public void processFile(String programFile) throws Exception {
        Scanner scanner = new Scanner(new File(programFile));
        while(scanner.hasNext()) {
            processLine(scanner.nextLine());
        }
    }

    //Firstly we split the line by space, after we will check if there are any other separators
    public void processLine(String line) {
        String[] atoms = line.split(" ");
        for(String atom: atoms) {
            obtainAtoms(atom);
        }
    }

    //After the are splitted completely, we check their type, if none of these types match, it is a lexical error
    public void processAtoms() throws AtomException {
        for(String atom: atoms) {
            if(codificationTable.checkIfKeyword(atom) != -2) {
                internalProgramForm.add(codificationTable.checkIfKeyword(atom),-1);
            } else if (codificationTable.checkIfOperator(atom) != -2) {
                internalProgramForm.add(codificationTable.checkIfOperator(atom),-1);
            } else if(codificationTable.checkIfSeparator(atom) != -2) {
                internalProgramForm.add(codificationTable.checkIfSeparator(atom),-1);
            } else if(atom.matches(codificationTable.identifierRegEx) && atom.length() <= 256) {
                symbolTable.addElement(atom);
                internalProgramForm.add(0,symbolTable.getElementPosition(atom));
            } else if(atom.matches(codificationTable.numberConstantRegEx) || atom.matches(codificationTable.stringConstantRegEx)) {
                symbolTable.addElement(atom);
                internalProgramForm.add(1,symbolTable.getElementPosition(atom));
            } else {
                throw new AtomException("Lexical error! " + atom);
            }
        }
        try {
            internalProgramForm.writeToFile();
            symbolTable.writeSymbolTableToFile();
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    //It takes an atom that was firstly splitted by space and checks if it contains any other separators or operators in order to split even more
    public void obtainAtoms(String atom){
        if(codificationTable.checkIfContainsSeparator(atom).compareTo("")==0 && codificationTable.checkIfContainsOperator(atom).compareTo("")==0){
            atoms.add(atom);
            return;
        } else {
            if(codificationTable.checkIfContainsSeparator(atom).compareTo("")!=0){
                String separator = codificationTable.checkIfContainsSeparator(atom);
                String sep = separator.replace(separator,"\\"+separator);
                String[] splitted = atom.split(sep);
                if(splitted.length > 0){
                    atoms.add(splitted[0]);
                }
                atoms.add(separator);
                if(splitted.length > 1){
                    obtainAtoms(splitted[1]);
                }
            } else if(codificationTable.checkIfContainsOperator(atom).compareTo("")!=0){
                String operator = codificationTable.checkIfContainsOperator(atom);
                String sep = operator.replace(operator,"\\"+operator);
                String[] splitted = atom.split(sep);
                if(splitted.length > 0){
                    atoms.add(splitted[0]);
                }
                atoms.add(operator);
                if(splitted.length > 1){
                    obtainAtoms(splitted[1]);
                }
            }

        }
    }
}
