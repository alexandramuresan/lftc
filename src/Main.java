import Algorithms.First;
import Algorithms.Follow;
import Algorithms.ParsingTable;
import Algorithms.ProductionString;
import Models.Grammar;
import Models.Production;
import Utilities.GrammarParser;
import Utilities.ProgramType;
import analizator.Processor;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
/*
        GrammarParser parser = new GrammarParser("grammar.txt");
        Grammar grammar = parser.getGrammar();
        List<Production> prod = grammar.getProductions();
        for(Production p : prod) {
            System.out.println(p.getTransitions().size());
        }

*/
        /*
        First first = new First();
        first.initializeFirst();
        first.computeFirst();
        System.out.println(first);
        */
/*
        Follow follow = new Follow.Builder()
                .grammar(grammar)
                .first(first)
                .follow(new HashMap<>())
                .build();
        System.out.println(follow);
        */

//        ProductionString ps = new ProductionString("a*a+a", ProgramType.SIMPLE_GRAMMAR);
//        try {
//            ps.processSequence();
//            System.out.println(ps.getProdNumbers());
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }

        try {
            Processor processor = new Processor("codification.txt","symbolTable.txt","ipf.txt","program.txt");
            processor.processAtoms();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }

        First first = new First(ProgramType.COMPLEX_GRAMMAR);
        System.out.println(first);

        GrammarParser parser = new GrammarParser("programGrammar.txt");
        Grammar grammar = parser.getGrammar();
        Follow follow = new Follow.Builder()
                .programType(ProgramType.COMPLEX_GRAMMAR)
                .grammar(grammar)
                .first(first)
                .follow(new HashMap<>())
                .build();
        System.out.println(follow);

        ParsingTable parsingTable = new ParsingTable(ProgramType.COMPLEX_GRAMMAR);
        System.out.println(parsingTable);

        ProductionString productionString = new ProductionString("start var a", ProgramType.COMPLEX_GRAMMAR);
        try {
            productionString.processSequence();
            System.out.println(productionString.getProdNumbers());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
